<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class School extends Model
{

	use SearchableTrait;

    public $table = 'schools';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    protected $searchable = [
        'columns' => [
            'schools.school_name' => 10,
            'schools.zipcode' => 5,
        ]
    ];

    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'school_name',
        'address',
        'zipcode',
        'web_address',
        'phone_number',
        'email',
        'hours',
        'fulltime',
        'partime',
        'age_range',
        'application_type',
        'application_fee',
        'application_fee_hide',
        'average_waitlist_time',
        'average_waitlist_time_hide',
        'application_due_date',
        'application_due_date_hide',
        'description',
        'status',
        'image',
        'stripe_account_id',
        'bank_account_id',
        'is_delete',
        'lat',
        'long',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
