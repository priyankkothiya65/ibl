<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;

class SchoolController extends Controller
{
    public function index(Request $request)
    {
    	if($request->method()=='GET'){
    		$schools = School::paginate(10);
        	return view('index', compact('schools'));
    	}else{
    		$searchName   = $request->search_name;
    		$searchZip    = $request->search_zip;
    		$filterOption = $request->filter_option;

    		if($searchName!='' || $searchZip!='')
    		{
    			$schools = School::latest()
				    		->where('school_name', 'like', '%' . $searchName . '%')
				    		->where('zipcode', 'like', '%' . $searchZip . '%') 
				    		->paginate(10);
				return view('index', compact('schools','searchName','searchZip','filterOption'));
    		}
    		else
    		{
    			if($filterOption=='ASC' || $filterOption=='DESC')
				{
					$schools = School::orderBy('school_name', $filterOption)->paginate(10);
				}
				else
				{
					$orderByPrice = 'DESC';
					if($filterOption=='price_asc')
					{
						$orderByPrice = 'ASC';
					}
					$schools = School::orderBy('application_fee', $orderByPrice)->paginate(10);	
				}	

	    		return view('index', compact('schools','searchName','searchZip','filterOption'));
    		}
    		
    	}
       
    }
}
